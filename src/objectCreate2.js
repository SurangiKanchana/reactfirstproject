import React from 'react'
import ReactDOM from 'react-dom';
import './index.css';


//set up vars
const title = 'This is my Title';
const author = 'This is the Author';

//we can pass the data to the boj in tag
function BookList() {
  return (
    <section className='booklist'>
      <Book job='developer' />
      <Book job='QA' name='surangi' age={25} />
    </section>
  )
}

//obj is a object
//to access the data pass to the obj: we can use <p>{obj.job}</p>
const Book = (obj) => {
  console.log(obj);
  return (
    <article className='book'>
      <Image />
      <Title />
      <Author />
      <p>{obj.job}</p>
      <p>{obj.age}</p>
    </article>
  )
}
const Image = () => {
  return <img src='https://images-na.ssl-images-amazon.com/images/I/51p2SDOCV9L._SX258_BO1,204,203,200_.jpg' alt='' />
}

const Title = () => {
  
  return (
    //data binding
  <h4>{title}</h4>
  )
}
/*in order to access the javascript world in the JSX, 
we need to add these curly braces*/ 
// Author = () => <h4 style={{color:'#617d98', fontSize: '0.75rem', marginTp: '0.25rem'}}>This is the Author</h4>

const Author = () => <h1>{author.toUpperCase()}</h1>

ReactDOM.render(<BookList />, document.getElementById('root'));