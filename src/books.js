//book object array
export const books = [
  {
  id: 1,
  img: 'https://images-na.ssl-images-amazon.com/images/I/51p2SDOCV9L._SX258_BO1,204,203,200_.jpg',
  title: 'I Love You To The Moon and Back',
  author: 'Amelia Hepworth',
},
  {
  id: 2,
  img: 'https://saintantoninus.org/thumb/photoalbums/our-class-is-a-family/IMG_3452.JPG',
  title: 'Our Class is a Family',
  author: 'jamie weaver-zimmerman',
},
  {
  id: 3,
  img: 'https://images-na.ssl-images-amazon.com/images/I/41ToPGNVNeL._SS400_.jpg',
  title: 'the vanishing half',
  author: 'jamie weaver-zimmerman',
},
];