/*
rfce + enter 
(ES7 React/Redux/GraphQL/React-Native snippets extension)
*/
import React from 'react'
import ReactDOM from 'react-dom';
/*
*
this is stateless function component
always must return JSX (JavaScript XML)
JSX is simply a syntax extension of JavaScript.
It allows us to directly write HTML in React (within JavaScript code).
this only return single elements use div/section/article/fragment
*
*/ 
//must start the function with capital letter
function Greeting(){
  return <h4>this is surangi and this is my first component</h4>;
}

/*****************************************************/
/*
function Greeting(){
  return (
    <div>
  <h4>hello world</h4>
  </div
  );
}

//*******this function is similar to ====> 

const Greeting = () => {
  return React.createElement(
    'div',
    {},
    React.creactElement('h1', {}, 'hello world')
  )
}
*/
/****************************************************/


/**********nested functions***************************
/*
function Greeting(){
  return (
    <div>
      <Person />
      <Message />
  </div
  );
}

const Person = () => <h2>surangi kanchana></h2>;
const Message = () => {
  return <p>This is my message</p>
};
*/
/****************************************************/


/*
rendering Greeting fuunction on index.html
we pass two attributes as 
what we are going to render (Greeting)
where we are going to render (document.getElementById('root')) 
*/
ReactDOM.render(<Greeting />, document.getElementById('root'));