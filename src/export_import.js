import React from 'react'
import ReactDOM from 'react-dom';
import './index.css';
import {books} from './books'
/*Book is a default export. 
so when we importing the Book the can be use 
import ChangedBook from './Book'; 
instead of 
import Book from './Book';*/
import Book from './Book';

function BookList() {
  return (
    <section className='booklist'>
      {books.map((book) => {
        console.log(book);
        return <Book key={book.id} {...book}></Book>;
      })}
    </section>
  )
}



ReactDOM.render(<BookList />, document.getElementById('root'));