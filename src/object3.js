import React from 'react'
import ReactDOM from 'react-dom';
import './index.css';

//create book objects
const firstBook = {
  img: 'https://images-na.ssl-images-amazon.com/images/I/51p2SDOCV9L._SX258_BO1,204,203,200_.jpg',
  title: 'I Love You To The Moon and Back',
  author: 'Amelia Hepworth',
}

const secondBook = {
  img: 'https://saintantoninus.org/thumb/photoalbums/our-class-is-a-family/IMG_3452.JPG',
  title: 'Our Class is a Family',
  author: 'jamie weaver-zimmerman',
}


//we can pass the data to the boj in tag
// to return p tag I add a attribute to the obj as children
function BookList() {
  return (
    <section className='booklist'>
      <Book
      img={firstBook.img}
      title={firstBook.title}
      author={firstBook.author}
      > 
      <p>
        Dummy para
      </p>
      </Book>
      <Book 
      img={secondBook.img}
      title={secondBook.title}
      author={secondBook.author} />
    </section>
  )
}

//first method
//obj is a object
//to access the data pass to the obj: we can use <p>{obj.job}</p>
/*
const Book = (obj) => {
  console.log(obj);
  return (
    <article className='book'>
      <img src={obj.img}></img>
      <h1>{obj.title}</h1>
      <h4>{obj.author}</h4>
    </article>
  )
}
*/

//second method
const Book = (obj) => {
  const { img, title, author,children } = obj
  console.log(obj);
  return (
    <article className='book'>
      <img src={img}></img>
      <h1>{title}</h1>
      <h4>{author}</h4>
      {children}
    </article>
  )
}

//third method
// children attribute help to return the p tag in BookList function
/*
const Book = ({ img, title, author, children }) => {
  return (
    <article className='book'>
      <img src={img}></img>
      <h1>{title}</h1>
      <h4>{author}</h4>
      {children}
    </article>
  )
}
*/

ReactDOM.render(<BookList />, document.getElementById('root'));