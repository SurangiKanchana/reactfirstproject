import React from 'react'
import ReactDOM from 'react-dom';
import './index.css';

//create book object array
const books = [
  {
  id: 1,
  img: 'https://images-na.ssl-images-amazon.com/images/I/51p2SDOCV9L._SX258_BO1,204,203,200_.jpg',
  title: 'I Love You To The Moon and Back',
  author: 'Amelia Hepworth',
},
  {
  id: 2,
  img: 'https://saintantoninus.org/thumb/photoalbums/our-class-is-a-family/IMG_3452.JPG',
  title: 'Our Class is a Family',
  author: 'jamie weaver-zimmerman',
},
  {
  id: 3,
  img: 'https://images-na.ssl-images-amazon.com/images/I/41ToPGNVNeL._SS400_.jpg',
  title: 'the vanishing half',
  author: 'jamie weaver-zimmerman',
},
];

//to wrap javascript in html we use map method
// const names = ['surangi', 'yasas'];
// const newNames = names.map((name)=>{
//   console.log(name)
//   return <h1>{name}</h1>
// });
// console.log(newNames);

//to spread a property we use ... 
function BookList() {
  return (
    <section className='booklist'>
      {/* {newNames} */}
      {books.map((book) => {
        console.log(book);
        // return <Book key={book.id} book={book}></Book>;
        return <Book key={book.id} {...book}></Book>;
      })}
    </section>
  )
}

const Book = (obj) => {
  //attribute, eventHandler
  //onClick, onMouseOver
const ClickHere = () => {
  alert('hello')
};
const ComplexButton = (author) => {
  console.log(author);
  alert(author);
};
  // const { img, title, author,children } = obj.book
  const { img, title, author } = obj;
  console.log(obj);

  return (
    <article className='book' onMouseOver={() => {
      console.log(title);
    }}>
      <img src={img}></img>
      <h1 onClick={() => console.log(title)}>{title}</h1>
      <h4>{author}</h4>
      <button type='button' onClick={ClickHere}>Click here</button>
      <button type='button' onClick={() => ComplexButton(author)}>Complex Button</button>
    </article>
  )
}

ReactDOM.render(<BookList />, document.getElementById('root'));