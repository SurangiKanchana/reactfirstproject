import React from 'react'

const Book = (obj) => {
const ClickHere = () => {
  alert('hello')
};
const ComplexButton = (author) => {
  console.log(author);
  alert(author);
};
  const { img, title, author } = obj;
  console.log(obj);

  return (
    <article className='book'>
      <img src={img}></img>
      <h1 onClick={() => console.log(title)}>{title}</h1>
      <h4>{author}</h4>
      <button type='button' onClick={ClickHere}>Click here</button>
      <button type='button' onClick={() => ComplexButton(author)}>Complex Button</button>
    </article>
  )
}

export default Book
